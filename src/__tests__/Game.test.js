import React from 'react';
import { shallow } from 'enzyme';
import { mount } from 'enzyme';
import { unmount } from 'enzyme';

import Game from './index.js';

/*
 I was having some trouble installing Jest and Enzyme.
 It threw some errors when I installed them
 and then my project wouldn't run
 so I uninstalled everything.
 
 I still wanted to try to write some unit tests.
 I can't run them but this is what I came up with
 from reading some articles and docs.
 
 I know there are a ton of tests that can be run.
 I'd like to get good at this.
 */

describe('Game component', () => {
         let wrapper;
         
         it('should render correctly', () => {
            expect(mount(<Game />)).toMatchSnapshot();
            });
         
         it('should render correctly', () => {
            wrapper = mount(<Board cards={cards} />);
            expect(wrapper.props().cards).to.have.lengthOf(52);
            wrapper.unmount();
            });
         
         it('should be possible to activate button with mouse click', () => {
            wrapper = mount(<MyComponent />);
            expect(wrapper.find('.newGame').simulate('click')).toHaveBeenCalled();
            wrapper.unmount();
            });
         });
