/*
 This was a fun coding challenge. I really enjoyed it!
 I've never done react before so I thought this was
 a good first project for me. In case you're wondering,
 Raeschel didn't help me at all. She didn't even want to
 see the finished project! But yeah I had a lot of fun
 doing this so thank you!
 */

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Card(props) {
    let button;
    if (props.visible) {
        button = <button
                    className="card"
                    onClick={props.onClick}>
                    <img src={props.cardClicked ? props.value : require('./images/playing-card-back.png')} alt="card"/>
                </button>
    } else {
        button = <button className="card"></button>
    }
    
    return (
        <div>{button}</div>
    );
}

class Board extends React.Component {
    renderCard(i) {
        return (
            <Card
                visible={this.props.visible[i]}
                cardClicked={this.props.cardsClicked[i]}
                value={this.props.allTheProps[i].image}
                onClick={() => this.props.onClick(i)}
            />
        );
    }
    
    createBoard = () => {
        let board = [];
        let columnSize = 13;
        let rowSize = 4;
        let cardId = 0;
        
        for (let i = 0; i < rowSize; i++) {
            let cards = [];
            for (let j = 0; j < columnSize; j ++) {
                cards.push(
                    <td key={j}>
                        {this.renderCard(cardId++)}
                    </td>
                );
            }
            board.push(
                <tr className="board-row" key={i}>
                    {cards}
                </tr>
            );
        }
        return board;
    }
    
    render() {
        return (
            <table>
            <tbody>
                {this.createBoard()}
            </tbody>
            </table>
        );
    }
}

class Game extends React.Component {
    constructor(props) {
        super(props);
        
        /*
         I'm not a big fan of all these arrays
         and I know there's a better way to do this
         but I couldn't get those other ways to work
         so this is what ended up working for me
        */
        this.state = {
            cards: Array(52).fill(null),
            cardsClicked: Array(52).fill(false),
            newButtonClicked: false,
            cardsClickedIndexes: [],
            visible: Array(52).fill(true),
            cardsDiscarded: 0,
            won: false
        };
    }
    
    handleClick(i) {
        const cards = this.state.cards.slice();
        const cardsClicked = this.state.cardsClicked.slice();
        const visible = this.state.visible.slice();
        let cardsClickedIndexes = this.state.cardsClickedIndexes.slice();
        let cardsDiscarded = this.state.cardsDiscarded;
        
        // Handle same card getting clicked and
        // more than two cards flipping
        if (cardsClicked[i] === false && cardsClickedIndexes.length < 2 ) {
            cardsClickedIndexes.push(i);
            cardsClicked[i] = true;
        }
        
        if (cardsClickedIndexes.length === 2) {
            // Delay for .5 sec so you see the card before it
            // flips back over or disappears
            delay(function() {
                  
            /*
             I wanted to use a for loop to iterate
             but for some reason the values wouldn't change.
             I also ran into this problem with the fetch results.
             I did some research and came upon map
             and if I had more time I would try that out.
            */
            if (cards[cardsClickedIndexes[0]].value === cards[cardsClickedIndexes[1]].value) {
                  visible[cardsClickedIndexes[0]] = false;
                  visible[cardsClickedIndexes[1]] = false;
                  cardsDiscarded += 2;
                  if (cardsDiscarded === 52) {
                    this.setState({
                        won: true
                    });
                  }
            } else {
                cardsClicked[cardsClickedIndexes[0]] = false;
                cardsClicked[cardsClickedIndexes[1]] = false;
            }
            cardsClickedIndexes = [];
                  
            this.setState({
                cardsClicked: cardsClicked,
                cardsClickedIndexes: cardsClickedIndexes,
                visible: visible,
                cardsDiscarded: cardsDiscarded
            });
                  
            }.bind(this), 500);
        }
        
        this.setState({
            cardsClicked: cardsClicked,
            cardsClickedIndexes: cardsClickedIndexes,
        });
    }
    
    fetch = () => {
        // Starting a new game so reset state
        this.setState({
            cards: Array(52).fill(null),
            cardsClicked: Array(52).fill(false),
            newButtonClicked: false,
            cardsClickedIndexes: [],
            visible: Array(52).fill(true),
            cardsDiscarded: 0,
            won: false
        });
        
        fetch("https://deckofcardsapi.com/api/deck/new/draw/?count=52")
        .then(results => results.json())
        .then(
            (result) => {
              /*
               I wanted to combine the cards, cardsClicked, and visible arrays
               so I had one cards array of objects
               but when I iterated the results and set the state,
               all the cards had the same value.
               I would alos like to try using map here too
               so I can get rid of all those arrays.
               */
              this.setState({
                    cards: result.cards,
                    newButtonClicked: true
                });
            },
        )
    }
    
    renderBoard() {
        const cards = this.state.cards.slice();
        const cardsClicked = this.state.cardsClicked.slice();
        const visible = this.state.visible.slice();
        
        return (
            <Board
                visible={visible}
                cardsClicked={cardsClicked}
                allTheProps={cards}
                onClick={(i) => this.handleClick(i)}
            />
        );
    }
    
    render() {
        let board;
        if (this.state.won) {
            board = <p className="winText">YOU WON!</p>
        } else {
            board = this.state.newButtonClicked ? this.renderBoard() : null
        }
        
        return (
            <div className="game">
                <div className="title">
                    <h1>CONCENTRATION</h1>
                </div>
                <div className="game-board">
                <button
                    className="newGame"
                    onClick={this.fetch}>
                    New Game
                </button>
                <br />
                {board}
                </div>
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <Game />,
    document.getElementById('root')
);

var delay = ( function() {
    var timer = 0;
    return function(callback, ms) {
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();
